use super::*;

pub fn simulate(
    time: Res<Time>,
    mut d_constraints: Query<&mut DistanceConstraint>,
    mut points: Query<(Entity, &mut Transform, &mut PrevPosition, &mut Point)>,
) {
    const ITERATIONS: i32 = 10;
    let dt = time.delta_seconds();
    if dt < f32::EPSILON {
        return;
    }

    for (_, mut trans, mut prev, point) in &mut points {
        prev.0 = trans.translation;
        // TODO: external forces
        trans.translation += dt * point.vel;
    }
    for mut c in &mut d_constraints {
        c.lambda = 0.0;
    }
    for _ in 0..ITERATIONS {
        for mut c in &mut d_constraints {
            let (dx0, dx1) = {
                let (_, t0, _, p0) = points.get(c.p0).unwrap();
                let (_, t1, _, p1) = points.get(c.p1).unwrap();
                let x0 = t0.translation;
                let x1 = t1.translation;
                let dlambda = c.dlambda(x0, p0, x1, p1, dt);
                c.lambda += dlambda;
                (
                    dlambda * p0.mass.inverse_or_0() * c.gradient_p0(x0, x1),
                    dlambda * p1.mass.inverse_or_0() * c.gradient_p1(x0, x1),
                )
            };
            points
                .get_component_mut::<Transform>(c.p0)
                .unwrap()
                .translation += dx0;
            points
                .get_component_mut::<Transform>(c.p1)
                .unwrap()
                .translation += dx1;
        }
    }
    for (_, trans, prev, mut point) in &mut points {
        point.vel = (trans.translation - prev.0) / dt;
    }
}
