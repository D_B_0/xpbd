use super::*;

pub fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let mesh = meshes.add(shape::Icosphere::default().try_into().unwrap());
    let material = materials.add(StandardMaterial::default());

    let points = [
        // (Vec3::new(0.0, 0.0, 0.0), Invertible::Finite(1.0)),
        (Vec3::new(1.0, 0.0, 0.0), Invertible::Finite(1.0)),
        (Vec3::new(0.0, 1.0, 0.0), Invertible::Finite(1.0)),
    ];
    let entities = points
        .iter()
        .copied()
        .map(|(position, mass)| {
            spawn_point(
                &mut commands,
                PointBuilder::default().mass(mass).build().unwrap(),
                position,
                mesh.clone(),
                material.clone(),
            )
        })
        .collect::<Vec<Entity>>();

    for i in 0..entities.len() {
        for j in (i + 1)..entities.len() {
            let l0 = (points[i].0 - points[j].0).length() * 0.5;
            commands.spawn(
                DistanceConstraintBuilder::default()
                    .point_0(entities[i])
                    .point_1(entities[j])
                    .rest_length(l0)
                    .stiffness(10.0)
                    .build()
                    .unwrap(),
            );
            info!(
                "Spawned constraint between {:?} and {:?}",
                entities[i], entities[j]
            );
        }
    }
}
