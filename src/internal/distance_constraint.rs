use super::*;

use derive_builder::Builder;

#[derive(Component, Builder, Clone)]
pub struct DistanceConstraint {
    #[builder(setter(name = "point_0"))]
    pub p0: Entity,
    #[builder(setter(name = "point_1"))]
    pub p1: Entity,
    #[builder(setter(name = "rest_length"))]
    l0: f32,
    #[builder(setter(into))]
    stiffness: Invertible,
    #[builder(setter(skip))]
    pub lambda: f32,
}

impl DistanceConstraint {
    fn c(&self, x0: Vec3, x1: Vec3) -> f32 {
        (x0 - x1).length() - self.l0
    }

    pub fn gradient_p0(&self, x0: Vec3, x1: Vec3) -> Vec3 {
        (x1 - x0).normalize_or_zero()
    }

    pub fn gradient_p1(&self, x0: Vec3, x1: Vec3) -> Vec3 {
        (x0 - x1).normalize_or_zero()
    }

    pub fn dlambda(&self, x0: Vec3, p0: &Point, x1: Vec3, p1: &Point, dt: f32) -> f32 {
        let alpha_tilde = self.stiffness.inverse_or_0() / (dt * dt);
        let denominator = p0.mass.inverse_or_0() * self.gradient_p0(x0, x1).length_squared()
            + p1.mass.inverse_or_0() * self.gradient_p1(x0, x1).length_squared()
            + alpha_tilde;
        if denominator.abs() <= f32::EPSILON {
            warn!("zero denominator in distance constraint");
            0.0
        } else {
            (self.c(x0, x1) - self.lambda * alpha_tilde) / denominator
        }
    }
}
