use super::*;

#[derive(Bundle, Clone)]
pub struct PointBundle {
    pub point: Point,
    pub prev_pos: PrevPosition,
    pub pbr_bundle: PbrBundle,
}
