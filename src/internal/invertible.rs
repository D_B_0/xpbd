#[allow(dead_code)]
#[derive(Clone, Copy)]
pub enum Invertible {
    Finite(f32),
    Infinite,
}

impl Invertible {
    pub fn inverse_or_0(&self) -> f32 {
        match self {
            Invertible::Finite(m) if m.abs() <= f32::EPSILON => 0.0,
            Invertible::Finite(m) => 1.0 / m,
            Invertible::Infinite => 0.0,
        }
    }
}

impl From<f32> for Invertible {
    fn from(value: f32) -> Self {
        Invertible::Finite(value)
    }
}
