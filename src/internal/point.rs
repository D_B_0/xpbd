use derive_builder::Builder;

use super::*;

#[derive(Component, Builder, Clone, Copy)]
pub struct Point {
    #[builder(setter(into))]
    pub mass: Invertible,
    #[builder(setter(skip))]
    pub vel: Vec3,
}

#[derive(Component, Clone, Copy)]
pub struct PrevPosition(pub Vec3);

pub fn spawn_point(
    commands: &mut Commands,
    point: Point,
    position: Vec3,
    mesh: Handle<Mesh>,
    material: Handle<StandardMaterial>,
) -> Entity {
    commands
        .spawn(PointBundle {
            point,
            prev_pos: PrevPosition(position),
            pbr_bundle: PbrBundle {
                mesh,
                material,
                transform: Transform::from_translation(position).with_scale(0.2 * Vec3::ONE),
                ..default()
            },
        })
        .id()
}
