use bevy::{
    render::{
        settings::{Backends, WgpuSettings},
        RenderPlugin,
    },
    window::WindowMode,
};
use bevy_flycam::prelude::*;

mod internal;
use internal::*;

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::Rgba {
            red: 0.2,
            green: 0.2,
            blue: 0.3,
            alpha: 1.0,
        }))
        .add_plugins(
            DefaultPlugins
                .set(WindowPlugin {
                    primary_window: Some(Window {
                        title: "Extended Position Based Dynamics".into(),
                        mode: WindowMode::Windowed,
                        ..default()
                    }),
                    ..default()
                })
                .set(RenderPlugin {
                    wgpu_settings: WgpuSettings {
                        backends: Some(Backends::DX12),
                        ..default()
                    },
                }),
        )
        .add_plugins(PlayerPlugin)
        .insert_resource(MovementSettings {
            sensitivity: 0.00015, // default: 0.00012
            speed: 4.0,           // default: 12.0
        })
        .add_systems(Startup, (maximize_window, rendering_stuff, setup))
        .add_systems(Update, simulate)
        .run();
}

fn maximize_window(mut windows: Query<&mut Window>) {
    windows.single_mut().set_maximized(true);
}

fn rendering_stuff(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    commands.spawn(PointLightBundle {
        point_light: PointLight {
            intensity: 9000.0,
            range: 100.,
            shadows_enabled: true,
            ..default()
        },
        transform: Transform::from_xyz(0.0, 16.0, 0.0),
        ..default()
    });

    // ground plane
    commands.spawn(PbrBundle {
        mesh: meshes.add(shape::Plane::from_size(50.0).into()),
        material: materials.add(Color::SILVER.into()),
        ..default()
    });
}
